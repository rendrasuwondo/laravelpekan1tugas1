<?php

trait Fight
{
    public  $attackPower, $defencePower;

    public function serang(Hewan $hewan)
    {
        $str = "{$this->nama} sedang menyerang {$hewan->nama}";

        $hewan->darah = $hewan->darah - $this->attackPower / $hewan->defencePower;

        return $str;
    }

    public function diserang(Hewan $hewan)
    {
        $str = "{$hewan->nama} sedang diserang";

        $this->darah = $this->darah - $hewan->attackPower / $this->defencePower;

        return $str;
    }
}

abstract class Hewan
{
    use Fight;



    public $nama,
        $darah = 50,
        $jumlah_kaki,
        $keahlian;


    public function __construct($nama = "nama", $darah = 50, $jumlah_kaki = 0, $keahlian = "")
    {
        $this->nama = $nama;
        $this->darah = $darah;
        $this->jumlah_kaki = $jumlah_kaki;
        $this->keahlian = $keahlian;
    }

    public function getLabel()
    {
        return "$this->nama";
    }

    abstract public function atraksi();
}
