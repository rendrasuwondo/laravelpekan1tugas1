<?php

require_once 'Hewan.php';
require_once 'Harimau.php';
require_once 'Elang.php';


$harimau_1 = new Harimau("Harimau_1", 50, 4, "Lari Cepat", 7, 8);
$elang_1 = new Elang("Elang_1", 50, 2, "Terbang Tinggi", 10, 5);

echo $harimau_1->getInfoHewan();
echo "<br>";
echo $elang_1->getInfoHewan();
echo "<br>";
echo $harimau_1->serang($elang_1);
echo "<br>";
echo $harimau_1->getInfoHewan();
echo "<br>";
echo $elang_1->getInfoHewan();
