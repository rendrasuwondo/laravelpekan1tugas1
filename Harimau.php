<?php

class Harimau extends Hewan
{

    public function __construct($nama = "nama", $darah = 50, $jumlah_kaki = 0, $keahlian = "lari_cepat", $attackPower = 7, $defencePower = 8)
    {
        $this->nama = $nama;
        $this->darah = $darah;
        $this->jumlah_kaki = $jumlah_kaki;
        $this->keahlian = $keahlian;
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower;
    }

    public function atraksi()
    {
        $str = "{$this->nama} sedang {$this->keahlian}";
        return $str;
    }


    public function getInfoHewan()
    {
        $str = "Nama : {$this->nama} | Jenis : Harimau | Darah : $this->darah | Jumlah Kaki : {$this->jumlah_kaki} | Keahlian : {$this->keahlian} | Attack Power : {$this->attackPower} | Defence Power : {$this->defencePower}";
        return $str;
    }
}
